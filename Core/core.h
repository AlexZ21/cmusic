#ifndef CORE_H
#define CORE_H

#include "core_global.h"
#include <ccore.h>

class CORESHARED_EXPORT Core : public CCore
{
    Q_OBJECT
public:
    Core(QObject *parent = nullptr);
    ~Core();
};

#endif // CORE_H

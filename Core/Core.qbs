import qbs

DynamicLibrary {
    name: "CMusicCore"
    files: [
        "core.cpp",
        "core.h",
        "core_global.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core"] }

    cpp.dynamicLibraries: [
        "CrnCore"
    ]

    cpp.libraryPaths: [
        "../../Crn/build/Core"
    ]

    cpp.includePaths: [
        "../../Crn/Core"
    ]

    cpp.defines: ["CORE_LIBRARY"]
    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlink", "dynamiclibrary_import"]
        qbs.install: true
        qbs.installDir: "../../../build/lib"
    }
}

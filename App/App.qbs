import qbs

CppApplication {
    name: "CMusic"
    type: "application"
    consoleApplication: false

    files: [
        "main.cpp",
        "musicapplication.cpp",
        "musicapplication.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core","gui","widgets"] }

    Depends { name: "CMusicCore" }

    cpp.includePaths: [
        "../Core",

        "../../Crn/Core"
    ]

    cpp.dynamicLibraries: [
        "CrnCore"
    ]

    cpp.libraryPaths: [
        "../../Crn/build/Core"
    ]

    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["application"]
        qbs.install: true
        qbs.installDir: "../../../build/bin"
    }

    Group {
            name: "Start script"
            files: "cmusic.sh"
            qbs.install: true
            qbs.installDir: "../../../build/bin"
    }
}

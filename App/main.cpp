#include "musicapplication.h"

int main(int argc, char *argv[])
{
    MusicApplication a(argc, argv);
    return a.exec();
}

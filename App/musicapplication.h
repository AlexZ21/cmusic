#ifndef MUSICAPPLICATION_H
#define MUSICAPPLICATION_H

#include <QApplication>

class Core;

class MusicApplication : public QApplication
{
    Q_OBJECT
public:
    MusicApplication(int &argc, char **argv);
    ~MusicApplication();

    void init();

    void initDefaultSettings();

private:
    Core *_core;

};

#endif // MUSICAPPLICATION_H

#include "musicapplication.h"

#include <core.h>
#include <ccorecomponent.h>
#include <cplugin.h>
#include <cres.h>
#include <clog.h>

#include <QTimer>
#include <QDebug>

MusicApplication::MusicApplication(int &argc, char **argv) :
    QApplication(argc, argv),
    _core(new Core())
{
    setApplicationName("CMusic");
    setApplicationVersion("1.0.0");
    setOrganizationName("AlexCrn");
    setOrganizationDomain("alexcrn.com");

    _core->setLogging(true);

    connect(_core, &Core::coreQuit, [=](){
        quit();
    });

    QTimer::singleShot(0, [=](){
        init();
    });
}

MusicApplication::~MusicApplication()
{
    delete _core;
}

void MusicApplication::init()
{
    // Load settings
    _core->loadSettingsFromFile("../share/cmusic/settings.ini");

    // Init default settings
    if (_core->settings()->allKeys().isEmpty())
        initDefaultSettings();

    // Set log file path
    if (_core->settings()->value("enableFileLog").toBool())
        CLog::setOutputFile(_core->settings()->value("fileLogPath").toString());

    // Loading plugins
    QStringList pluginsPaths = _core->settings()->value("pluginsPaths").toStringList();
    for (const QString &pluginsPath : pluginsPaths) {
        _core->registerPluginsDirectoryAsync(pluginsPath, [this](){
            _core->initPluginsAsync([this](){
                cDebug() << "HUESOS";
            });
        });
    }
}

void MusicApplication::initDefaultSettings()
{
    _core->settings()->setValue("enableFileLog", true);
    _core->settings()->setValue("fileLogPath", "../share/cmusic/log.txt");

    _core->settings()->setValue("pluginsPaths", QStringList() << "../share/cmusic/plugins");
    _core->settings()->setValue("enablePlugins", QStringList() << "*");
}

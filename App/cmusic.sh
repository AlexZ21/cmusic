#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

LIB_DIR="$DIR/../lib"
CRN_LIB_DIR="$DIR/../../../Crn/build/Core"

echo $DIR
echo $LIB_DIR
echo $CRN_LIB_DIR

export LD_LIBRARY_PATH="$LIB_DIR:$CRN_LIB_DIR"

"$DIR/CMusic" "$@"


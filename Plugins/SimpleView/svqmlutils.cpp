#include "svqmlutils.h"

#include <QCursor>

QPoint SVQmlUtils::mouseGlobalPos()
{
    return QCursor::pos();
}

#ifndef SVWNDFRAME_H
#define SVWNDFRAME_H

#include <QWidget>

class QGraphicsOpacityEffect;

class SVWndFrame : public QWidget
{
    Q_OBJECT
public:
    explicit SVWndFrame(QWidget *parent = 0);

    QWidget *centralWidget() const;
    void setCentralWidget(QWidget *centralWidget);

protected:
    void resizeEvent(QResizeEvent *event);

private:
    void updateMask();
signals:

public slots:

private:
    QWidget *_bg;
    QWidget *_centralWidget;
    QGraphicsOpacityEffect *_centralWidgetOpacityEffect;
};

#endif // SVWNDFRAME_H

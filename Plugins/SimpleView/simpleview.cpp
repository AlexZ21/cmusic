#include "simpleview.h"
#include "viewcmp.h"
#include "svrect.h"
#include "svqmlutils.h"
#include "svcorewrap.h"

#include <ccore.h>
#include <clog.h>
#include <cres.h>

#include <QQmlApplicationEngine>

SimpleView::SimpleView() :
    CPlugin(cPluginInfo()),
    _viewCmp(new ViewCmp())
{
    _viewCmp->moveToThread(QCoreApplication::instance()->thread());
}

SimpleView::~SimpleView()
{
}

CRes SimpleView::init()
{
    qmlRegisterType<SVRect>("SimpleView", 1, 0, "SVRect");
    qmlRegisterType<SVQmlUtils>("SimpleView", 1, 0, "SVUtils");
    qmlRegisterType<SVCoreWrap>("SimpleView", 1, 0, "SVCore");

    core()->addComponent(_viewCmp);

    return CResSuccess();
}


#ifndef SVQMLUTILS_H
#define SVQMLUTILS_H

#include <QObject>
#include <QPoint>

class SVQmlUtils : public QObject
{
    Q_OBJECT

public:
    SVQmlUtils() : QObject() {}

    Q_INVOKABLE QPoint mouseGlobalPos();
};

#endif // SVQMLUTILS_H

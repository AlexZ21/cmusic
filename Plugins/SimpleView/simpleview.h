#ifndef SIMPLEVIEW_H
#define SIMPLEVIEW_H

#include "simpleview_global.h"

#include <cplugin.h>

class ViewCmp;

class SIMPLEVIEWSHARED_EXPORT SimpleView : public CPlugin
{
public:
    SimpleView();
    ~SimpleView();

protected:
    CRes init();

protected:
    ViewCmp *_viewCmp;

};

C_EXPORT_PLUGIN(SimpleView, // Class name
                "SimpleView", // Plugin name
                "Simple view plugin for CMusic", // Plugin description
                "1.0", // Plugin version
                QStringList(), // Plugin depends
                QStringList() << "*") // Init before

#endif // SIMPLEVIEW_H

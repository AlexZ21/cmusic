#include "svwndframe.h"

//#include <QPaintEvent>
#include <QPainter>
//#include <QStyleOption>
#include <QPixmap>
#include <QBitmap>
#include <QImage>
#include <QGraphicsOpacityEffect>
#include <QStyleOptionFocusRect>
#include <QDebug>

SVWndFrame::SVWndFrame(QWidget *parent) : QWidget(parent), _centralWidget(nullptr)
{
    resize(500, 400);
    _bg = new QWidget(this);
    _bg->setStyleSheet(".QWidget{border: 1px solid white; border-radius: 5px; background-color: black;}");
    _centralWidget = new QWidget(_bg);

    setAttribute(Qt::WA_TranslucentBackground);
//    setWindowFlags(Qt::X11BypassWindowManagerHint);
//    setWindowFlags(Qt::FramelessWindowHint);

    setAutoFillBackground(false);

    updateMask();
}

void SVWndFrame::resizeEvent(QResizeEvent *event)
{
    _bg->setGeometry(20, 20, width() - 40, height() - 40);
    _centralWidget->setGeometry(1, 1, _bg->width()-2, _bg->height()-2);
    updateMask();
}

void SVWndFrame::updateMask()
{
    QImage alpha_mask(_centralWidget->size(), QImage::Format_ARGB32);
    alpha_mask.fill(Qt::transparent);
    QPainter p(&alpha_mask);
    p.setRenderHint(QPainter::Antialiasing);
    QPainterPath path;
    path.addRoundedRect(alpha_mask.rect(), 4, 4);
    p.fillPath(path, Qt::white);
    p.drawPath(path);

    _centralWidget->setMask(QPixmap::fromImage(alpha_mask).mask());
}

QWidget *SVWndFrame::centralWidget() const
{
    return _centralWidget;
}


#include "svcorewrap.h"

#include <core.h>

#include <QDebug>

SVCoreWrap::SVCoreWrap(QObject *parent) : QObject(parent)
{

}

void SVCoreWrap::setCore(Core *core)
{
    _core = core;

    connect(_core, &Core::currentPluginInitialize, [this](const QString &pluginName){
        emit initializePlugin(pluginName);
    });
}

import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: item

    property alias source: mask.source
    property alias radius: rect.radius

    Rectangle {
        id: rect
        anchors.fill: parent
        smooth: true
        visible: false
    }

    OpacityMask {
        id: mask
        anchors.fill: parent
        maskSource: rect
    }
}

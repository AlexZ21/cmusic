import QtQuick 2.0
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import "./styles"

Window {
    id: root
    visible: true
    flags: csd ? Qt.FramelessWindowHint : Qt.Window
    color: csd ? "transparent" : "white"

    property QtObject style: WindowStyle {}
    property bool csd: true
    property alias toolbar: windowToolbar.children

    MouseMovement {
        id: mouseMovement

        globalMousePosition: true
        control: root
        frameWidth: rootItem.shadowRadius
        visible: csd

        property bool rootItem: true

        onDoubleClicked: root.visibility = root.visibility == Window.Windowed ?
                             Window.Maximized : Window.Windowed;

        onPositionChanged: {
            if (pressedButtons == Qt.LeftButton) {
                if (root.visibility == Window.Maximized)
                    root.visibility = Window.Windowed
            }
        }
    }

    RectangleShadow {
        id: rootItem
        anchors.fill: parent
        cornerRadius: csd && root.visibility != Window.Maximized ? style.cornerRadius * Screen.devicePixelRatio + 1 : 0
        shadowRadius: style.shadowRadius * Screen.devicePixelRatio
        shadowOpacity: style.shadowOpacity
        color: style.background
        shadow: csd && root.visibility != Window.Maximized

        property bool rootItem: true

        RoundRectangleMask {
            source: windowContent
            anchors.fill: parent
            radius: csd && root.visibility != Window.Maximized ? style.cornerRadius * Screen.devicePixelRatio : 0
            visible: csd && root.visibility != Window.Maximized
        }

        Item {
            id: windowContent
            anchors.fill: parent
            clip: true
            opacity: !(csd && root.visibility != Window.Maximized)
        }

        RowLayout {
            id: windowHeader
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: style.windowHeader * Screen.devicePixelRatio
            layoutDirection: style.controlsDirection
            clip: true

            MouseArea {
                height: parent.height
                width: style.controlSize*3 + style.controlSideOffset*2
                hoverEnabled: true
                visible: csd

                onEntered: {
                    closeControl.imageOpacity = style.closeControlImageOpacity;
                    maximizeControl.imageOpacity = style.maximizeControlImageOpacity;
                    minimizeControl.imageOpacity = style.minimizeControlImageOpacity;
                }
                onExited: {
                    closeControl.imageOpacity = 0;
                    maximizeControl.imageOpacity = 0;
                    minimizeControl.imageOpacity = 0;
                }

                RowLayout {
                    id: windowControls
                    height: parent.height
                    width: style.controlSize*3 + style.controlSpacing*2
                    clip: true
                    spacing: style.controlSpacing
                    anchors.centerIn: parent
                    layoutDirection: style.controlsDirection

                    WindowControlButton {
                        id: closeControl
                        width: style.controlSize * Screen.devicePixelRatio
                        height: width
                        imageSource: style.closeControlImageSource
                        imageScale: 0.5
                        imageOpacity: 0
                        bgDefaultColor: style.closeControlBgDefaultColor
                        bgHoveredColor: style.closeControlBgHoveredColor
                        bgBorderDefaultColor: style.closeControlBgBorderDefaultColor
                        bgBorderDefaultWidth: 0
                        bgBorderHoveredWidth: 0
                        anchors.verticalCenter: parent.verticalCenter

                        Behavior on imageOpacity {
                            NumberAnimation { duration: 100 }
                        }

                        onClicked: root.close()
                    }

                    WindowControlButton {
                        id: maximizeControl
                        width: style.controlSize * Screen.devicePixelRatio
                        height: width
                        imageSource: style.maximizeControlImageSource
                        imageScale: 0.5
                        imageOpacity: 0
                        bgDefaultColor: style.maximizeControlBgDefaultColor
                        bgHoveredColor: style.maximizeControlBgHoveredColor
                        bgBorderDefaultColor: style.maximizeControlBgBorderDefaultColor
                        bgBorderDefaultWidth: 0
                        bgBorderHoveredWidth: 0
                        anchors.verticalCenter: parent.verticalCenter

                        Behavior on imageOpacity {
                            NumberAnimation { duration: 100 }
                        }

                        onClicked: root.visibility = root.visibility == Window.Windowed ?
                                       Window.Maximized : Window.Windowed;
                    }

                    WindowControlButton {
                        id: minimizeControl
                        width: style.controlSize * Screen.devicePixelRatio
                        height: width
                        imageSource: style.minimizeControlImageSource
                        imageScale: 0.5
                        imageOpacity: 0
                        bgDefaultColor: style.minimizeControlBgDefaultColor
                        bgHoveredColor: style.minimizeControlBgHoveredColor
                        bgBorderDefaultColor: style.minimizeControlBgBorderDefaultColor
                        bgBorderDefaultWidth: 0
                        bgBorderHoveredWidth: 0
                        anchors.verticalCenter: parent.verticalCenter

                        Behavior on imageOpacity {
                            NumberAnimation { duration: 100 }
                        }

                        onClicked: root.visibility = Window.Minimized
                    }
                }
            }

            Item {
                id: windowToolbar
                Layout.fillWidth: true
                height: parent.height
            }
        }


    }

    contentItem.onChildrenChanged: updateChildren()
    function updateChildren() {
        for (var i = 0; i < contentItem.children.length; i++) {
            if (!contentItem.children[i].rootItem)
                contentItem.children[i].parent = windowContent;
//            if (typeOf(contentItem.children[i], "WindowScreen"))
//                contentItem.children[i].window = root;
        }
    }
//    function typeOf(obj, className) {
//      var str = obj.toString();
//      return str.indexOf(className + "(") === 0 || str.indexOf(className + "_QML") === 0;
//    }
}

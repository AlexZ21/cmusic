import QtQuick 2.0
import QtGraphicalEffects 1.0

Item {
    id: root

    property bool shadow: true
    property alias shadowRadius: glow.glowRadius
    property alias shadowOpacity: glow.opacity
    property alias shadowColor: glow.color
    property alias color: rect.color
    property alias cornerRadius: rect.radius

    RectangularGlow {
        id: glow
        anchors.fill: rect
        color: "#000000"
        opacity: 0.15
        cornerRadius: rect.radius
        visible: shadow

        property bool rootItem: true
    }

    Rectangle {
        id: rect
        anchors.fill: parent
        anchors.margins: glow.visible ? glow.glowRadius : 0
        clip: true
        property bool rootItem: true
    }

    onChildrenChanged: updateChildren()
    function updateChildren() {
        for (var i = 0; i < root.children.length; i++) {
            if (!root.children[i].rootItem)
                root.children[i].parent = rect;
        }
    }
}

import QtQuick 2.2

Item {
    id: root

    signal clicked
    signal entered
    signal exited

    Accessible.role: Accessible.Button
    Accessible.onPressAction: {
        root.clicked()
    }

    MouseArea {
        anchors.fill: parent
        cursorShape: Qt.PointingHandCursor
        onClicked: parent.clicked()
        hoverEnabled: true
        onEntered: parent.entered()
        onExited: parent.exited()
    }

    Keys.onSpacePressed: clicked()
}

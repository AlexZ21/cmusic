import QtQuick 2.2
import "."

MouseArea {
    id: mouseControl
    anchors.fill: parent
    hoverEnabled: true

    // Координаты верхнего левого угла окна.
    property variant leftTopCorner
    // Координаты нижнего правогоугла окна.
    property variant rightBottomCorner
    // Новые координаты левого верхнего угла окна.
    property variant newLeftTopCorner
    // Новый размер окна.
    property variant newSize
    // Рамки окна
    // 0 - верхний левый угл
    // 1 - верхняя сторона
    // 2 - правый верхний угл
    // 3 - правая сторона
    // 4 - правый нижний угл
    // 5 - нижняя сторона
    // 6 - левый нижний угл
    // 7 - левая сторона
    // 8 - середина
    property int sectionMousePressed
    // Нажата левая кнопка мыши.
    property bool mousePressed: false
    // Координаты мыши в окне при нажатии.
    property variant pressMousePosition
    // Координаты мыши на эране.
    property variant newMousePosition
    // Ширина рамки
    property int frameWidth: 12
    property bool globalMousePosition: false
    property var control: mouseControl.parent

    onPressed: {
        // Запоминаем координаты нажатия мыши.
        pressMousePosition = Qt.point(mouseX, mouseY);
        // Запоминаем координаты левого верхнего угла.
        leftTopCorner = Qt.point(control.x, control.y);
        // Запоминаем координаты нижнего правого угла.
        rightBottomCorner = Qt.point(control.x + control.width, control.y + control.height);
        // Устанавливаем флаг нажатия мыши.
        mousePressed = true;
    }

    onReleased: {
        // Снимаем флаг нажатия мыши.
        mousePressed = false;
    }

    onPositionChanged: {
        // При измененнии позиции мыши получаем ее координаты на экране.
        if (!globalMousePosition)
            newMousePosition = Qt.point(mouseX, mouseY);
        else
            newMousePosition = svUtils.mouseGlobalPos();

        // Если кнопка мыши не нажата, то проверяем ее координаты относительно окна.
        // В зависимости от положения мыши устанавливаем иконку курсора и
        // зону, в которой находится мышь.
        if (!mousePressed) {
            if (mouseX < frameWidth) {
                if (mouseY < frameWidth) {
                    // Левый верхний угл.
                    cursorShape = Qt.SizeFDiagCursor;
                    sectionMousePressed = 0;
                } else if (mouseY > (control.height - frameWidth)) {
                    // Левый нижний угл.
                    cursorShape = Qt.SizeBDiagCursor;
                    sectionMousePressed = 6;
                } else {
                    // Левая сторона.
                    cursorShape = Qt.SizeHorCursor;
                    sectionMousePressed = 7;
                }

            } else if (mouseX > (control.width - frameWidth)) {
                if (mouseY < frameWidth) {
                    // Правый верхний угл.
                    cursorShape = Qt.SizeBDiagCursor;
                    sectionMousePressed = 2;
                } else if (mouseY > (control.height - frameWidth)) {
                    // Правый нижний угл.
                    cursorShape = Qt.SizeFDiagCursor;
                    sectionMousePressed = 4;
                } else {
                    // Правая сторона.
                    cursorShape = Qt.SizeHorCursor;
                    sectionMousePressed = 3;
                }

            } else if (mouseY < frameWidth) {
                if (mouseX < frameWidth) {
                    // Левый верхний угл.
                    cursorShape = Qt.SizeFDiagCursor;
                    sectionMousePressed = 0;
                } else if (mouseX > (control.width - frameWidth)) {
                    // Првый верхний угл.
                    cursorShape = Qt.SizeBDiagCursor;
                    sectionMousePressed = 2;
                } else {
                    // Верхняя сторона.
                    cursorShape = Qt.SizeVerCursor;
                    sectionMousePressed = 1;
                }

            }  else if (mouseY > (control.height - frameWidth)) {


                if (mouseX < frameWidth) {
                    // Правый нижний угл.
                    cursorShape = Qt.SizeBDiagCursor;
                    sectionMousePressed = 4;
                } else if (mouseX > (control.width - frameWidth)) {
                    // Левый нижний угл.
                    cursorShape = Qt.SizeFDiagCursor;
                    sectionMousePressed = 6;
                } else {
                    // Нижняя сторона.
                    cursorShape = Qt.SizeVerCursor;
                    sectionMousePressed = 5;
                }

            } else {
                // Центр контейнера.
                cursorShape = Qt.ArrowCursor;
                sectionMousePressed = 8;
            }
        } else {
            // Если зажата кнопка мыши, то меняем размер окна или его координаты.
            switch (sectionMousePressed) {
            case 0: // Левый верхний угл.
                newLeftTopCorner = newMousePosition;
                newSize = Qt.size(rightBottomCorner.x - newLeftTopCorner.x,
                                        rightBottomCorner.y - newLeftTopCorner.y);
                geometryChangeTimer.start();
                break;
            case 1: // Верхняя сторона.
                newLeftTopCorner = Qt.point(leftTopCorner.x, newMousePosition.y);
                newSize = Qt.size(control.width,
                                        rightBottomCorner.y - newLeftTopCorner.y);
                geometryChangeTimer.start();
                break;
            case 2: // Правый верхний угл.
                newLeftTopCorner = Qt.point(leftTopCorner.x,
                                                  newMousePosition.y);
                newSize = Qt.size(newMousePosition.x - newLeftTopCorner.x,
                                        rightBottomCorner.y - newLeftTopCorner.y);
                geometryChangeTimer.start();
                break;
            case 3: // Правая сторона.
                newLeftTopCorner = leftTopCorner;
                newSize = Qt.size(newMousePosition.x - newLeftTopCorner.x,
                                        control.height);
                geometryChangeTimer.start();
                break;
            case 4: // Правый нижний угл.
                newLeftTopCorner = leftTopCorner;
                newSize = Qt.size(newMousePosition.x - newLeftTopCorner.x,
                                        newMousePosition.y - newLeftTopCorner.y);
                geometryChangeTimer.start();
                break;
            case 5: // Нижняя сторона.
                newLeftTopCorner = leftTopCorner;
                newSize = Qt.size(control.width,
                                        newMousePosition.y - newLeftTopCorner.y);
                geometryChangeTimer.start();
                break;
            case 6: //Нижний левый угл.
                newLeftTopCorner = Qt.point(newMousePosition.x,
                                                  leftTopCorner.y);
                newSize = Qt.size(rightBottomCorner.x - newLeftTopCorner.x,
                                        newMousePosition.y - newLeftTopCorner.y);
                geometryChangeTimer.start();
                break;
            case 7: // Левая сторона.
                newLeftTopCorner = Qt.point(newMousePosition.x,
                                                  leftTopCorner.y);
                newSize = Qt.size(rightBottomCorner.x - newLeftTopCorner.x,
                                        control.height);
                geometryChangeTimer.start();
                break;
            case 8: // Центр окна.
                // Меня координаты окна в зависимости от положения курсора.
                cursorShape = Qt.ClosedHandCursor
                if (pressedButtons == Qt.LeftButton) {
                    control.x = newMousePosition.x - pressMousePosition.x;
                    control.y = newMousePosition.y - pressMousePosition.y;
                }
                break;
            }

        }
    }

    // Таймер, по которому изменяется геометрия окна.
    // Нужно для того что бы не дергалось окно при изменении размера окна
    Timer {
        id: geometryChangeTimer
        interval: 20;
        running: false;
        repeat: false
        onTriggered: {
            mouseControl.control.x = newLeftTopCorner.x;
            mouseControl.control.y = newLeftTopCorner.y;
            mouseControl.control.width = newSize.width;
            mouseControl.control.height = newSize.height;
        }
    }
}

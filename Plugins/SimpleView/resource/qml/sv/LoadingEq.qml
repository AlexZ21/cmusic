import QtQuick 2.0
import QtQuick.Layouts 1.1

RowLayout {
    id: root

    property int count: 5
    property int columnWidth: 4
    property string columnColor: "white"
    property int columnMinHeight: height/2
    property int columnMaxHeight: height

    Component {
        id: columnComponent

        Rectangle {
            id: column
            height: columnMinHeight
            width: columnWidth
            implicitWidth: width
            implicitHeight: height
            Layout.alignment: Qt.AlignBottom// | Qt.AlignHCenter
            color: columnColor

            Behavior on height {
                NumberAnimation {
                    duration: 400
                    running: false
                    easing.type: Easing.OutQuad
                    onRunningChanged: {
                        if (!running)
                            if (column.height === columnMinHeight)
                                column.height = columnMaxHeight;
                            else
                                column.height = columnMinHeight;
                    }
                }
            }
        }
    }

    Component.onCompleted: {
        for (var i = 0; i < count; ++i)
            var o = columnComponent.createObject(root);
    }


    Timer {
        interval: 220;
        running: true;
        repeat: true;

        property int current: 0

        onTriggered: {
            root.children[current].height = columnMaxHeight;
            ++current;
            if (current == count )
                stop();
        }
    }
}


import QtQuick 2.0

Item {
    id: root

    property var window

    Item {
        id: content
        anchors.fill: parent
    }
}

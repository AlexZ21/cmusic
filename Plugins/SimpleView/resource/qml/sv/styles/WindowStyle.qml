import QtQuick 2.0

Style {
    property color background: "#FFFFFF"
    property int cornerRadius: 4
    property int shadowRadius: 15
    property real shadowOpacity: 0.5

    property int windowHeader: 44

    property int controlSpacing: 8
    property int controlSideOffset: 22
    property int controlSize: 16
    property int controlsDirection: Qt.LeftToRight

    property string closeControlBgDefaultColor: "#fc5753"
    property string closeControlBgHoveredColor: "#bf4744"
    property string closeControlBgBorderDefaultColor: "#bf4744"
    property string closeControlImageSource: "qrc:/SimpleView/resource/imgs/close.svg"
    property real closeControlImageOpacity: 0.6

    property string maximizeControlBgDefaultColor: "#33c748"
    property string maximizeControlBgHoveredColor: "#299738"
    property string maximizeControlBgBorderDefaultColor: "#299738"
    property string maximizeControlImageSource: "qrc:/SimpleView/resource/imgs/maximize.svg"
    property real maximizeControlImageOpacity: 0.6

    property string minimizeControlBgDefaultColor: "#fdbc40"
    property string minimizeControlBgHoveredColor: "#be8d2f"
    property string minimizeControlBgBorderDefaultColor: "#be8d2f"
    property string minimizeControlImageSource: "qrc:/SimpleView/resource/imgs/minimize.svg"
    property real minimizeControlImageOpacity: 0.6
}

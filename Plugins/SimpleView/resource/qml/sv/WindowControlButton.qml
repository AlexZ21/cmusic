import QtQuick 2.0

Button {
    id: root

    property string bgDefaultColor: "transparent"
    property string bgHoveredColor: "transparent"
    property string bgBorderDefaultColor: "transparent"
    property string bgBorderHoveredColor: "transparent"
    property int bgBorderDefaultWidth: 0
    property int bgBorderHoveredWidth: 0
    property real imageScale: 1
    property alias imageOpacity: img.opacity
    property alias imageSource: img.source

    Rectangle {
        id: bg
        anchors.fill: parent
        radius: width/2
        color: bgDefaultColor
        border.color: bgBorderDefaultColor
        border.width: bgBorderDefaultWidth

        Behavior on color {
            ColorAnimation { duration: 100 }
        }

        Behavior on border.color {
            ColorAnimation { duration: 100 }
        }

        Behavior on border.width {
            NumberAnimation { duration: 100 }
        }
    }

    Image {
        id: img
        sourceSize: Qt.size(root.width * imageScale, root.height * imageScale)
        anchors.centerIn: parent
    }

    onEntered: {
        bg.color = bgHoveredColor;
        bg.border.color = bgBorderHoveredColor;
        bg.border.width = bgBorderHoveredWidth;
    }
    onExited: {
        bg.color = bgDefaultColor;
        bg.border.color = bgBorderDefaultColor;
        bg.border.width = bgBorderDefaultWidth;
    }
}

import QtQuick 2.0
import "./sv"
import "./sv/styles"

Item {
    property QtObject windowStyle: WindowStyle {}

    Image {
        id: splashImage
        anchors.fill: parent
        source: "qrc:/SimpleView/resource/imgs/splash.jpg"
        fillMode: Image.PreserveAspectCrop
    }

    Image {
        id: bgDotted
        anchors.fill: parent
        source: "qrc:/SimpleView/resource/imgs/dotted_pattern.png"
        fillMode: Image.Tile
    }

    Text {
        id: appNameText
        text: qsTr("CMusic")
        color: "white"
        y: windowStyle.windowHeader
        x: windowStyle.controlSideOffset / 2
        font.pixelSize: 50
        opacity: 1
    }

    Text {
        id: appVersionText
        text: qsTr("0.1.0001")
        color: "white"
        y: windowStyle.windowHeader + 56
        x: windowStyle.controlSideOffset / 1.6
        font.pixelSize: 14
        opacity: 0.8
    }

    LoadingEq {
        anchors.bottom: parent.bottom
        anchors.bottomMargin: windowStyle.controlSideOffset / 1.6
        x: windowStyle.controlSideOffset / 1.6
        width: 18
        height: 14
        count: 4
        spacing: 2
        columnWidth: 2
        opacity: 0.8
    }

    Text {
        id: initPluginText
        text: qsTr("Инициализация плагина")
        color: "white"
        anchors.bottom: parent.bottom
        anchors.bottomMargin: windowStyle.controlSideOffset / 1.9
        x: windowStyle.controlSideOffset / 1.6 + 24
        font.pixelSize: 12
        opacity: 0.8



        function updateText(newText) {
            text = newText;
        }
    }

    Component.onCompleted: {
        svCore.initializePlugin.connect(initPluginText.updateText);
    }

}

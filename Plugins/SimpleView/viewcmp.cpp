#include "viewcmp.h"
#include "svwndframe.h"
#include "svrect.h"
#include "svqmlutils.h"
#include "svcorewrap.h"

#include <core.h>
#include <ccore.h>
#include <clog.h>
#include <cres.h>

#include <QUrl>
#include <QQmlContext>
#include <QApplication>
#include <QThread>

ViewCmp::ViewCmp() : QObject(), CCoreComponent("View")
{
}

ViewCmp::~ViewCmp()
{
    delete _svUtils;
    delete _svCore;
    delete _engine;
}

CRes ViewCmp::init()
{
    QObject o;
    connect(&o, &QObject::destroyed, this, &ViewCmp::init_s, Qt::BlockingQueuedConnection);
    return _initRes;
}

void ViewCmp::init_s()
{
    _svUtils = new SVQmlUtils();
    _svCore = new SVCoreWrap();
    _svCore->setCore(dynamic_cast<Core*>(core()));

    _engine = new QQmlEngine();
    _rootComponent = new QQmlComponent(_engine);

    _engine->rootContext()->setContextProperty("svUtils", _svUtils);
    _engine->rootContext()->setContextProperty("svCore", _svCore);

    _rootComponent->loadUrl(QUrl("qrc:/SimpleView/resource/qml/main.qml"));
    _rootComponent->create();

    _initRes = CResSuccess();
}

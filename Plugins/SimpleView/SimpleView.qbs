import qbs

DynamicLibrary {
    name: "SimpleView"
    files: [
        "resource.qrc",
        "simpleview.cpp",
        "simpleview.h",
        "simpleview_global.h",
        "svcorewrap.cpp",
        "svcorewrap.h",
        "svqmlutils.cpp",
        "svqmlutils.h",
        "svrect.cpp",
        "svrect.h",
        "viewcmp.cpp",
        "viewcmp.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui", "qml", "quick", "widgets", "quickwidgets"] }

    cpp.dynamicLibraries: [
        "CrnCore",
        "CMusicCore"
    ]

    cpp.libraryPaths: [
        "../../../Crn/build/Core",
        "../../build/lib"
    ]

    cpp.includePaths: [
        "../../../Crn/Core",
        "../../Core"
    ]

    cpp.defines: ["SIMPLEVIEW_LIBRARY"]
    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlink", "dynamiclibrary_import"]
        qbs.install: true
        qbs.installDir: "../../../build/share/cmusic/plugins"
    }
}

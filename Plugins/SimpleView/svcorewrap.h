#ifndef SVCOREWRAP_H
#define SVCOREWRAP_H

#include <QObject>

class Core;

class SVCoreWrap : public QObject
{
    Q_OBJECT
public:
    explicit SVCoreWrap(QObject *parent = 0);

    void setCore(Core *core);

signals:
    void initializePlugin(const QString &pluginName);

private:
    Core *_core;
};

#endif // SVCOREWRAP_H

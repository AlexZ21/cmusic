#ifndef VIEWCMP_H
#define VIEWCMP_H

#include <ccorecomponent.h>

#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlEngine>
#include <QQmlComponent>
#include <QEvent>

#define GuiCreateEvent QEvent::User

class SVQmlUtils;
class SVCoreWrap;

class ViewCmp : public QObject, public CCoreComponent
{
    Q_OBJECT
public:
    ViewCmp();
    ~ViewCmp();

protected:
    CRes init();

//    bool event(QEvent *event);

public slots:
    void init_s();


private:
    QQmlEngine *_engine;
    QQmlComponent *_rootComponent;

    SVQmlUtils *_svUtils;
    SVCoreWrap *_svCore;

    CRes _initRes;
};

#endif // VIEWCMP_H

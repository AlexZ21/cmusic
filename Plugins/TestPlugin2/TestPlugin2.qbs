import qbs

DynamicLibrary {
    name: "TestPlugin2"
    files: [
        "testplugin2.cpp",
        "testplugin2.h",
        "testplugin2_global.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }

    cpp.dynamicLibraries: [
        "CrnCore"
    ]

    cpp.libraryPaths: [
        "../../../Crn/build/Core"
    ]

    cpp.includePaths: [
        "../../../Crn/Core"
    ]

    cpp.defines: ["TESTPLUGIN2_LIBRARY"]
    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlink", "dynamiclibrary_import"]
        qbs.install: true
        qbs.installDir: "../../../build/share/cmusic/plugins"
    }
}

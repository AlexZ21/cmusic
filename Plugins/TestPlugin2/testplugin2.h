#ifndef TESTPLUGIN2_H
#define TESTPLUGIN2_H

#include "testplugin2_global.h"

#include <cplugin.h>

class TESTPLUGIN2SHARED_EXPORT TestPlugin2 : public CPlugin
{
public:
    TestPlugin2();
    ~TestPlugin2();

protected:
    CRes init();
};

C_EXPORT_PLUGIN(TestPlugin2, // Class name
                "TestPlugin2", // Plugin name
                "Super puper plugin 2", // Plugin description
                "1.0", // Plugin version
                QStringList(), // Plugin depends
                QStringList()) // Init before



#endif // TESTPLUGIN_H

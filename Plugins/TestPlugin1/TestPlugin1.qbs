import qbs

DynamicLibrary {
    name: "TestPlugin1"
    files: [
        "testplugin1.cpp",
        "testplugin1.h",
        "testplugin1_global.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core"] }

    cpp.dynamicLibraries: [
        "CrnCore"
    ]

    cpp.libraryPaths: [
        "../../../Crn/build/Core"
    ]

    cpp.includePaths: [
        "../../../Crn/Core"
    ]

    cpp.defines: ["TESTPLUGIN1_LIBRARY"]
    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlink", "dynamiclibrary_import"]
        qbs.install: true
        qbs.installDir: "../../../build/share/cmusic/plugins"
    }
}

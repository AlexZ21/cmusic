#ifndef TESTPLUGIN1_H
#define TESTPLUGIN1_H

#include "testplugin1_global.h"

#include <cplugin.h>

class TESTPLUGIN1SHARED_EXPORT TestPlugin1 : public CPlugin
{
public:
    TestPlugin1();
    ~TestPlugin1();

protected:
    CRes init();

};

C_EXPORT_PLUGIN(TestPlugin1, // Class name
                "TestPlugin1", // Plugin name
                "Super puper plugin 1", // Plugin description
                "1.0", // Plugin version
                QStringList() << "TestPlugin2", // Plugin depends
                QStringList()) // Init before


#endif // TESTPLUGIN1_H

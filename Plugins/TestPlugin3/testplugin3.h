#ifndef TESTPLUGIN3_H
#define TESTPLUGIN3_H

#include "testplugin3_global.h"

#include <cplugin.h>

class TESTPLUGIN3SHARED_EXPORT TestPlugin3 : public CPlugin
{
public:
    TestPlugin3();
    ~TestPlugin3();

protected:
    CRes init();
};

C_EXPORT_PLUGIN(TestPlugin3, // Class name
                "TestPlugin3", // Plugin name
                "Super puper plugin 3", // Plugin description
                "1.0", // Plugin version
                QStringList(), // Plugin depends
                QStringList()) // Init before


#endif // TESTPLUGIN3_H

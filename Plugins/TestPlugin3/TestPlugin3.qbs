import qbs

DynamicLibrary {
    name: "TestPlugin3"
    files: [
        "testplugin3.cpp",
        "testplugin3.h",
        "testplugin3_global.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core"] }

    cpp.dynamicLibraries: [
        "CrnCore"
    ]

    cpp.libraryPaths: [
        "../../../Crn/build/Core"
    ]

    cpp.includePaths: [
        "../../../Crn/Core"
    ]

    cpp.defines: ["TESTPLUGIN3_LIBRARY"]
    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlink", "dynamiclibrary_import"]
        qbs.install: true
        qbs.installDir: "../../../build/share/cmusic/plugins"
    }
}

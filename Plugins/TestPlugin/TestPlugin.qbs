import qbs

DynamicLibrary {
    name: "TestPlugin"
    files: [
        "testplugin.cpp",
        "testplugin.h",
        "testplugin_global.h",
    ]

    Depends { name: "cpp" }
    Depends { name: "Qt"; submodules: ["core", "gui", "widgets"] }

    cpp.dynamicLibraries: [
        "CrnCore"
    ]

    cpp.libraryPaths: [
        "../../../Crn/build/Core"
    ]

    cpp.includePaths: [
        "../../../Crn/Core"
    ]

    cpp.defines: ["TESTPLUGIN_LIBRARY"]
    cpp.cxxLanguageVersion: ["c++11"]

    Group {
        fileTagsFilter: ["dynamiclibrary", "dynamiclibrary_symlink", "dynamiclibrary_import"]
        qbs.install: true
        qbs.installDir: "../../../build/share/cmusic/plugins"
    }
}

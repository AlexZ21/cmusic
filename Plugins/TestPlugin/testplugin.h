#ifndef TESTPLUGIN_H
#define TESTPLUGIN_H

#include "testplugin_global.h"

#include <cplugin.h>
#include <QString>

class TESTPLUGINSHARED_EXPORT TestPlugin : public CPlugin
{
public:
    TestPlugin();
    ~TestPlugin();

protected:
    CRes init();
};

C_EXPORT_PLUGIN(TestPlugin, // Class name
                "TestPlugin", // Plugin name
                "Super puper plugin", // Plugin description
                "1.0", // Plugin version
                QStringList() << "TestPlugin2" << "TestPlugin3", // Plugin depends
                QStringList()) // Init before

#endif // TESTPLUGIN_H
